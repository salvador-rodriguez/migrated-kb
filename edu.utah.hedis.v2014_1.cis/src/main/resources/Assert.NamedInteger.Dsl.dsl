[then] Assert.NamedInteger - Assert name and value for Named Integer {NAME} for both Rules and Process = 

String nO_{NAME} = (namedObjects.get("{NAME}")).toString();

String nOAsString_{NAME} = nO_{NAME}.toString();

assertions.add("{NAME}=" + nOAsString_{NAME});

Assertion $assertion_nOAsString_{NAME}  = new Assertion("{NAME}=" + nOAsString_{NAME});

insert($assertion_nOAsString_{NAME}); //DslUsed==Assert.NamedInteger.Dsl|||NAME=={NAME}