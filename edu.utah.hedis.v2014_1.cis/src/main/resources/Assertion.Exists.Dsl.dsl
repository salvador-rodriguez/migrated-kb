[when] Assertion.Exists - There is assertion {ASSERTION:ENUM:Assertion.value} = 
(
$assertion_{ASSERTION} : Assertion(value == "{ASSERTION}")
) //DslUsed==Assertion.Exists.Dsl|||Assertion=={ASSERTION}