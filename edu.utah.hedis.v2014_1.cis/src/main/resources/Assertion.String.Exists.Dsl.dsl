[when] Assertion.String.Exists - There is an Assertion String {ASSERTIONSTRING} = 
(
	//$assertions_{ASSERTIONSTRING} : Assertion
	Assertion
	(
		value == "{ASSERTIONSTRING}"
	)
) //DslUsed==Assertion.String.Exists|||ASSERTIONSTRING=={ASSERTIONSTRING}