package edu.utah.hedis.v2014_1.cis;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.lang.Integer;
import org.opencds.common.ConsoleLogger;
import org.opencds.common.ILogger;
import org.opencds.common.utilities.AbsoluteTimeDifference;
import org.opencds.common.utilities.DateUtility;
import org.opencds.common.utilities.MiscUtility;
import org.opencds.common.utilities.StreamUtility;
import org.opencds.common.utilities.StringToArrayUtility;
import org.opencds.common.utilities.StringUtility;
import org.opencds.common.utilities.XMLDateUtility;
import org.opencds.vmr.v1_0.internal.utilities.LogicHelperUtility;
import org.opencds.vmr.v1_0.internal.AdministrableSubstance;
import org.opencds.vmr.v1_0.internal.AdverseEvent;
import org.opencds.vmr.v1_0.internal.AdverseEventBase;
import org.opencds.vmr.v1_0.internal.AppointmentProposal;
import org.opencds.vmr.v1_0.internal.AppointmentRequest;
import org.opencds.vmr.v1_0.internal.Assertion;
import org.opencds.vmr.v1_0.internal.BodySite;
import org.opencds.vmr.v1_0.internal.CDSContext;
import org.opencds.vmr.v1_0.internal.CDSInput;
import org.opencds.vmr.v1_0.internal.CDSOutput;
import org.opencds.vmr.v1_0.internal.CDSResource;
import org.opencds.vmr.v1_0.internal.ClinicalStatement;
import org.opencds.vmr.v1_0.internal.ClinicalStatementRelationship;
import org.opencds.vmr.v1_0.internal.Demographics;
import org.opencds.vmr.v1_0.internal.DeniedAdverseEvent;
import org.opencds.vmr.v1_0.internal.DeniedProblem;
import org.opencds.vmr.v1_0.internal.DoseRestriction;
import org.opencds.vmr.v1_0.internal.DosingSigElement;
import org.opencds.vmr.v1_0.internal.EncounterBase;
import org.opencds.vmr.v1_0.internal.EncounterEvent;
import org.opencds.vmr.v1_0.internal.Entity;
import org.opencds.vmr.v1_0.internal.EntityBase;
import org.opencds.vmr.v1_0.internal.EntityRelationship;
import org.opencds.vmr.v1_0.internal.EvalTime;
import org.opencds.vmr.v1_0.internal.EvaluatedPerson;
import org.opencds.vmr.v1_0.internal.EvaluatedPersonRelationship;
import org.opencds.vmr.v1_0.internal.EvaluatedPersonAgeAtEvalTime;
import org.opencds.vmr.v1_0.internal.Facility;
import org.opencds.vmr.v1_0.internal.FocalPersonId;
import org.opencds.vmr.v1_0.internal.Goal;
import org.opencds.vmr.v1_0.internal.GoalBase;
import org.opencds.vmr.v1_0.internal.GoalProposal;
import org.opencds.vmr.v1_0.internal.GoalValue;
import org.opencds.vmr.v1_0.internal.MissedAppointment;
import org.opencds.vmr.v1_0.internal.NamedObject;
import org.opencds.vmr.v1_0.internal.ObservationBase;
import org.opencds.vmr.v1_0.internal.ObservationOrder;
import org.opencds.vmr.v1_0.internal.ObservationProposal;
import org.opencds.vmr.v1_0.internal.ObservationResult;
import org.opencds.vmr.v1_0.internal.ObservationValue;
import org.opencds.vmr.v1_0.internal.Organization;
import org.opencds.vmr.v1_0.internal.OtherEvaluatedPerson;
import org.opencds.vmr.v1_0.internal.Person;
import org.opencds.vmr.v1_0.internal.Problem;
import org.opencds.vmr.v1_0.internal.ProblemBase;
import org.opencds.vmr.v1_0.internal.ProcedureBase;
import org.opencds.vmr.v1_0.internal.ProcedureEvent;
import org.opencds.vmr.v1_0.internal.ProcedureOrder;
import org.opencds.vmr.v1_0.internal.ProcedureProposal;
import org.opencds.vmr.v1_0.internal.RelationshipToSource;
import org.opencds.vmr.v1_0.internal.ScheduledAppointment;
import org.opencds.vmr.v1_0.internal.ScheduledProcedure;
import org.opencds.vmr.v1_0.internal.Specimen;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationBase;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationEvent;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationOrder;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationProposal;
import org.opencds.vmr.v1_0.internal.SubstanceDispensationEvent;
import org.opencds.vmr.v1_0.internal.SupplyBase;
import org.opencds.vmr.v1_0.internal.SupplyEvent;
import org.opencds.vmr.v1_0.internal.SupplyOrder;
import org.opencds.vmr.v1_0.internal.SupplyProposal;
import org.opencds.vmr.v1_0.internal.UnconductedObservation;
import org.opencds.vmr.v1_0.internal.UndeliveredProcedure;
import org.opencds.vmr.v1_0.internal.UndeliveredSubstanceAdministration;
import org.opencds.vmr.v1_0.internal.UndeliveredSupply;
import org.opencds.vmr.v1_0.internal.VMR;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.CDSInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.ClinicalStatementInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.CodedAttributeRequirement;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.EvaluatedPersonInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.PatientInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.RelatedEntityInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.RelatedEvaluatedPersonInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.TimeAttributeRequirement;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventAffectedBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventAffectedBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventAgentConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventSeverityConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventStatusConcept;
import org.opencds.vmr.v1_0.internal.concepts.BrandedMedicationConcept;
import org.opencds.vmr.v1_0.internal.concepts.CDSInputTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.CDSOutputTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.ClinicalStatementRelationshipConcept;
import org.opencds.vmr.v1_0.internal.concepts.ClinicalStatementTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.DataSourceTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.DoseTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.DosingSigConcept;
import org.opencds.vmr.v1_0.internal.concepts.EncounterCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.EncounterTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.EntityRelationshipConcept;
import org.opencds.vmr.v1_0.internal.concepts.EntityTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.EntityTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.EthnicityConcept;
import org.opencds.vmr.v1_0.internal.concepts.EvaluatedPersonRelationshipConcept;
import org.opencds.vmr.v1_0.internal.concepts.GenderConcept;
import org.opencds.vmr.v1_0.internal.concepts.GenericMedicationConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalCodedValueConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalFocusConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalStatusConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ImmunizationConcept;
import org.opencds.vmr.v1_0.internal.concepts.InformationAttestationTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.InformationRecipientPreferredLanguageConcept;
import org.opencds.vmr.v1_0.internal.concepts.InformationRecipientTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.ManufacturerConcept;
import org.opencds.vmr.v1_0.internal.concepts.MedicationClassConcept;
import org.opencds.vmr.v1_0.internal.concepts.MedicationConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationCodedValueConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationFocusConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationInterpretationConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationMethodConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationUnconductedReasonConcept;
import org.opencds.vmr.v1_0.internal.concepts.PreferredLanguageConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemAffectedBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemAffectedBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemImportanceConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemSeverityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemStatusConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureApproachBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureApproachBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureMethodConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.RaceConcept;
import org.opencds.vmr.v1_0.internal.concepts.ResourceTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationApproachBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationApproachBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationGeneralPurposeConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceDeliveryMethodConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceDeliveryRouteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceFormConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SystemUserPreferredLanguageConcept;
import org.opencds.vmr.v1_0.internal.concepts.SystemUserTaskContextConcept;
import org.opencds.vmr.v1_0.internal.concepts.SystemUserTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.UndeliveredProcedureReasonConcept;
import org.opencds.vmr.v1_0.internal.concepts.UndeliveredSubstanceAdministrationReasonConcept;
import org.opencds.vmr.v1_0.internal.concepts.VmrOpenCdsConcept;
import org.opencds.vmr.v1_0.internal.concepts.VMRTemplateConcept;
import org.opencds.vmr.v1_0.internal.datatypes.AddressPartType;
import org.opencds.vmr.v1_0.internal.datatypes.ADXP;
import org.opencds.vmr.v1_0.internal.datatypes.ANY;
import org.opencds.vmr.v1_0.internal.datatypes.AD;
import org.opencds.vmr.v1_0.internal.datatypes.BL;
import org.opencds.vmr.v1_0.internal.datatypes.CD;
import org.opencds.vmr.v1_0.internal.datatypes.CS;
import org.opencds.vmr.v1_0.internal.datatypes.EntityNamePartQualifier;
import org.opencds.vmr.v1_0.internal.datatypes.EntityNamePartType;
import org.opencds.vmr.v1_0.internal.datatypes.EntityNameUse;
import org.opencds.vmr.v1_0.internal.datatypes.EN;
import org.opencds.vmr.v1_0.internal.datatypes.ENXP;
import org.opencds.vmr.v1_0.internal.datatypes.II;
import org.opencds.vmr.v1_0.internal.datatypes.INT;
import org.opencds.vmr.v1_0.internal.datatypes.IVLDate;
import org.opencds.vmr.v1_0.internal.datatypes.IVLPQ;
import org.opencds.vmr.v1_0.internal.datatypes.IVLINT;
import org.opencds.vmr.v1_0.internal.datatypes.IVLQTY;
import org.opencds.vmr.v1_0.internal.datatypes.IVLREAL;
import org.opencds.vmr.v1_0.internal.datatypes.IVLRTO;
import org.opencds.vmr.v1_0.internal.datatypes.PostalAddressUse;
import org.opencds.vmr.v1_0.internal.datatypes.PQ;
import org.opencds.vmr.v1_0.internal.datatypes.QTY;
import org.opencds.vmr.v1_0.internal.datatypes.REAL;
import org.opencds.vmr.v1_0.internal.datatypes.RTO;
import org.opencds.vmr.v1_0.internal.datatypes.TEL;
import org.opencds.vmr.v1_0.internal.datatypes.TelecommunicationAddressUse;
import org.opencds.vmr.v1_0.internal.datatypes.TelecommunicationCapability;
import org.opencds.vmr.v1_0.internal.datatypes.UncertaintyType;
import org.opencds.vmr.v1_0.internal.datatypes.XP;

function boolean dateListsFulfillDistinctDateCountCriteria(java.util.List dateList1, java.util.List dateList2, int minCount1, int minCount2) 
{
/* 
Returns true if dateList1 contains at least minCount1 distinct dates and dateList2 contains at least minCount2 distinct dates, where:
- Distinct date means not on same day as any other date in dateList1 or dateList2 (ignores hours, minutes and seconds)
- A same date in both dateList1 and dateList2 may be counted towards one and only one of the list counts

E.g., 
dateList1 is 1/1/11, 1/2/11, 1/2/11, 1/3/11
dateList2 is 1/3/11, 1/4/11, 1/4/11

dateList1 has 3 distinct dates - 1/1/11, 1/2/11, 1/3/11
dateList2 has 2 distinct dates - 1/3/11, 1/4/11

1/3/11 is shared by both lists but may only be counted towards one.  

This function first counts up distinct dates in each list that has no overlap with the other.

dateList1 has 2 distinct dates with no overlap with dateList2 - 1/1/11, 1/2/11
dateList2 has 1 distinct date with no overlap with dateList1 - 1/4/11

Then, for dates that overlap (in this case 1/3/11), it is added to dateList1 if dateList1 has not yet fulfilled its minCount.  If it has, the dates that overlap are added to the dateList2 count.  This approach ensures that the potential for returning true to the function is maximized.
*/

	// first, convert null lists to be empty lists
	if (dateList1 == null)
	{
		dateList1 = new java.util.ArrayList();
	}
	
	if (dateList2 == null)
	{
		dateList2 = new java.util.ArrayList();
	}

	// Identify the distinct dates in each list
	
	// these lists contains only dates which have different dates (regardless of hours, minutes and seconds) compared to other dates already included
	java.util.ArrayList<java.util.Date> distinctDateList1 = new java.util.ArrayList<java.util.Date>();
	java.util.ArrayList<java.util.Date> distinctDateList2 = new java.util.ArrayList<java.util.Date>();

	org.opencds.common.utilities.DateUtility dateUtility = org.opencds.common.utilities.DateUtility.getInstance();
	
	for (java.util.Date date1 : (java.util.List<java.util.Date>) dateList1)
	{
		boolean dateAlreadyIncluded1 = false;
		for (java.util.Date distinctDateAlreadyInList1 : distinctDateList1)
		{
			if(dateUtility.isSameDay(date1, distinctDateAlreadyInList1))
			{
				dateAlreadyIncluded1 = true;
			}
		}
		if (! dateAlreadyIncluded1)
		{
			// add version with no time components
			distinctDateList1.add(stripTimeComponent(date1));
		}
	}
	
	for (java.util.Date date2 : (java.util.List<java.util.Date>) dateList2)
	{
		boolean dateAlreadyIncluded2 = false;
		for (java.util.Date distinctDateAlreadyInList2 : distinctDateList2)
		{
			if(dateUtility.isSameDay(date2, distinctDateAlreadyInList2))
			{
				dateAlreadyIncluded2 = true;
			}
		}
		if (! dateAlreadyIncluded2)
		{
			// add version with no time components
			distinctDateList2.add(stripTimeComponent(date2));
		}
	}
	
	// Identify dates that overlap in the two lists and add them to the common list
	
	java.util.ArrayList<java.util.Date> distinctDateListOverlap = new java.util.ArrayList<java.util.Date>();
	
	for (int i = 0; i < distinctDateList1.size(); i++)
	{
		java.util.Date distinctDate1 = distinctDateList1.get(i);
		
		for (int j = 0; j < distinctDateList2.size(); j++)
		{
			
			java.util.Date distinctDate2 = distinctDateList2.get(j);
			
			if(dateUtility.isSameDay(distinctDate1, distinctDate2))
			{				
				distinctDateListOverlap.add(distinctDate1);
			}
		}
	}
	
	// Create new lists that do not contain the overlapping dates	
	java.util.ArrayList<java.util.Date> distinctDateList1NoOverlap = new java.util.ArrayList<java.util.Date>();
	java.util.ArrayList<java.util.Date> distinctDateList2NoOverlap = new java.util.ArrayList<java.util.Date>();
	
	for (java.util.Date date1 : distinctDateList1)
	{
		if (! distinctDateListOverlap.contains(date1))
		{
			distinctDateList1NoOverlap.add(date1);
		}
	}
	
	for (java.util.Date date2 : distinctDateList2)
	{
		if (! distinctDateListOverlap.contains(date2))
		{
			distinctDateList2NoOverlap.add(date2);
		}
	}
	
	int list1Count = distinctDateList1NoOverlap.size();
	int list2Count = distinctDateList2NoOverlap.size();
	int overlapCount = distinctDateListOverlap.size();
	
	for (int k = 0; k < overlapCount; k++)
	{
		if (list1Count < minCount1)
		{
			list1Count++;
		}
		else
		{
			list2Count++;
		}
	}
	
	if((list1Count >= minCount1) && (list2Count >= minCount2))
	{
		return true;
	}
	else
	{
		return false;
	}
}	