[when] Dem.Age - Evaluated Person age is {COMP:ENUM:Comparison.operator} to {N} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnit} = 
(
	EvaluatedPersonAgeAtEvalTime(
	personId == $evaluatedPersonId, 
	ageUnit == "{TIMEUNITS}", 
	age {COMP} {N})
) 
//DslUsed==Dem.Age.Dsl|||N=={N}|||COMP=={COMP}|||TIMEUNITS=={TIMEUNITS}