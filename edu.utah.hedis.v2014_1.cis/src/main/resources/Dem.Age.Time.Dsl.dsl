[when] Dem.Age.Time - Evaluated Person age was {COMP:ENUM:Comparison.operator} {N1} {TIMEUNITS1:ENUM:EnumerationTarget.javaCalendarUnit} at {N2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnit} {PLUSMINUS:ENUM:Comparison.plusMinus} {N3} {TIMEUNITS3:ENUM:EnumerationTarget.javaCalendarUnit} before evalTime, where time components are ignored if age unit is year, month, or day  = 
(
/*
	E.g., Evaluated Person age was [>=] [18][yr] at [8][mo] [+] [1][d] before evalTime, where time is ignored if age unit is yr, month, or day
*/ 
	EvaluatedPerson
	(
		id == $evaluatedPersonId, 
		demographics != null,
		eval(getAgeInTimeUnitAtTime(demographics.getBirthTime(), org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime(org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime($evalTime, {TIMEUNITS2}, -1 * {N2}), {TIMEUNITS3}, -1 * {PLUSMINUS}1 * {N3}), {TIMEUNITS1}) {COMP} {N1})
	)
) 
//DslUsed==Dem.Age.Time.Dsl||COMP=={COMP}|||N1=={N1}|||TIMEUNITS1=={TIMEUNITS1}|||N2=={N2}|||TIMEUNITS2=={TIMEUNITS2}|||PLUSMINUS=={PLUSMINUS}|||N3=={N3}|||TIMEUNITS3=={TIMEUNITS3}