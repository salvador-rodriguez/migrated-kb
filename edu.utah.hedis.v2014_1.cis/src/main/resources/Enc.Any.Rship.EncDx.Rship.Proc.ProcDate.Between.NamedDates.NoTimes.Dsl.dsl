[when] Enc.Any.Rship.EncDx.Rship.Proc.ProcDate.Between.NamedDates.NoTimes - Evaluated Person had an encounter with procedure {HIGHLOW:ENUM:TimeInterval.highLowUpper} time {COMP1:ENUM:Comparison.operatorGTE} {NAMEDDATE1NAME} and {COMP2:ENUM:Comparison.operatorLTE} {NAMEDDATE2NAME} where {PROB:ENUM:ProblemConcept.openCdsConceptCode} was {CSREL1:ENUM:ClinicalStatementRelationshipConcept.openCdsConceptCode} the encounter and where {PROC:ENUM:ProcedureConcept.openCdsConceptCode} was {CSREL2:ENUM:ClinicalStatementRelationshipConcept.openCdsConceptCode} the encounter ignoring time components of all dates = 
(
/*
ProbConcept --> ProbId ------|              
                             --> CSRel1SourceId ------------------------|
CSRel1Concept --> CSRel1Id --|                                           |  
                                                                          |
ProcConcept --> ProcConceptTargetId ----|                                 -> Encounter*
                                        --> ProcId -|                     |
Eval Person ID, Proc Date Restrictions -|            |--> CSRel2SourceId |
                                                     |
CSRel2Concept --> CSRel2Id -------------------------|

ProbId --> Problem*
ProcId --> Procedure*
EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}
*flagged for return
       	
*/
	$NamedDate1_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : NamedDate
	(
		name == "{NAMEDDATE1NAME}", 
		date != null,
		$NamedDate1Value_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE1NAME}"))
	) and

	$NamedDate2_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : NamedDate
	(
		name == "{NAMEDDATE2NAME}", 
		date != null,
		$NamedDate2Value_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE2NAME}"))
	) and
	
	$Encounter_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : EncounterEvent
	(
//		id == $CSRel1SourceId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2},
//		id == $CSRel2SourceId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}
	) and
	
	$CSR1_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : ClinicalStatementRelationship
	( 
//		id == $CSRel2Id_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2},
//		targetId == $ProcId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2},
//		$CSRel2SourceId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : sourceId
		sourceId == $Encounter_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.id
	) and
		
	ClinicalStatementRelationshipConcept
	( 
		openCdsConceptCode == "{CSREL2}",
//		$CSRel2Id_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : conceptTargetId 
		conceptTargetId == $CSR1_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.id
	) and
	
	$Problem_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : Problem
	(
//		id == $ProbId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}
		id == $CSR1_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.targetId
	) and
	
	ProblemConcept
	( 
		openCdsConceptCode == "{PROB}",
//		$ProbId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : conceptTargetId,
		conceptTargetId == $Problem_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.id
	) and  
	
	$CSR2_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : ClinicalStatementRelationship
	( 
//		id == $CSRel1Id_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2},
//		targetId == $ProbId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2},
//		$CSRel1SourceId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : sourceId
		sourceId == $Encounter_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.id
	) and
		
	ClinicalStatementRelationshipConcept
	( 
		openCdsConceptCode == "{CSREL1}",
//		$CSRel1Id_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : conceptTargetId 
		conceptTargetId == $CSR2_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.id
	) and	

	$Procedure_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : ProcedureEvent
	(
//		$ProcId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : id == $ProcConceptTargetId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2},
		id == $CSR2_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.targetId,
		evaluatedPersonId == $evaluatedPersonId, 
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP1} (stripTimeComponent($NamedDate1Value_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2})),
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP2} (stripTimeComponent($NamedDate2Value_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2})) 		
	) and
	
	ProcedureConcept
	( 
		openCdsConceptCode == "{PROC}",
//		$ProcConceptTargetId_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2} : conceptTargetId 
		conceptTargetId == $Procedure_EAREDRPBNDNT_{HIGHLOW}{PROB}{CSREL1}{PROC}{CSREL2}.id
	) 

) //DslUsed==Enc.Any.Rship.EncDx.Rship.Proc.ProcDate.Between.NamedDates.NoTimes.Dsl|||HIGHLOW=={HIGHLOW}|||COMP1=={COMP1}|||NAMEDDATE1NAME=={NAMEDDATE1NAME}|||COMP2=={COMP2}|||NAMEDDATE2NAME=={NAMEDDATE2NAME}|||PROB=={PROB}|||CSREL1=={CSREL1}|||PROC=={PROC}|||CSREL2=={CSREL2}