[when] Enc.Any.Time - Evaluated Person had any encounter whose {HIGHLOW:ENUM:TimeInterval.highLowUpper} time was in the past {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(

	$EncAnyTimeDsl_encounterEvent_{INT}{TIMEUNITS} : EncounterEvent
	(
	evaluatedPersonId == $evaluatedPersonId,  
	eval(timeBeforeByAtMost(encounterEventTime.get{HIGHLOW}(), $evalTime, {INT}, {TIMEUNITS}, namedObjects))
	) and

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($EncAnyTimeDsl_encounterEvent_{INT}{TIMEUNITS})))
) //DslUsed==Enc.Any.Time.Dsl|||INT=={INT}|||HIGHLOW=={HIGHLOW}|||TIMEUNITS=={TIMEUNITS}