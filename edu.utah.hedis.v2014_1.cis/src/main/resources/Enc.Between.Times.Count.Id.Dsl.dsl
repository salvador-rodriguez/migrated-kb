[when] Enc.Between.Times.Count.Id - Evaluated Person had at least {INT1} {ENCLISTID:ENUM:EncounterListId.reference} {ENCTYPE:ENUM:EncounterTypeConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time at most {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} and more than {INT3} {TIMEUNITS3:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} ago = 
(
	$EBTCI_EncTypeConceptTargetIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3} : java.util.List (size >= {INT1} ) from accumulate 
	( 
		EncounterTypeConcept
		( 
		openCdsConceptCode == "{ENCTYPE}",
		$EBTCI_EncTypeConceptTargetId_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3} : conceptTargetId 
		),
	init (ArrayList $EBTCI_TempEncTypeConceptTargetIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3} = new ArrayList(); ),
	action ($EBTCI_TempEncTypeConceptTargetIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}.add($EBTCI_EncTypeConceptTargetId_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}); ),
	reverse ($EBTCI_TempEncTypeConceptTargetIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}.remove($EBTCI_EncTypeConceptTargetId_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}); ),
	result($EBTCI_TempEncTypeConceptTargetIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3})
	) and  
	
	$EBTCI_FinalEncIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3} : java.util.List (size >= {INT1} ) from accumulate 
	( 
		EncounterEvent
		( 
			$EBTCI_FinalEncId_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3} : id memberOf $EBTCI_EncTypeConceptTargetIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}, 
			evaluatedPersonId == $evaluatedPersonId, 
			eval(timeBeforeByAtMost(encounterEventTime.get{HIGHLOW}(), $evalTime, {INT2}, {TIMEUNITS2}, namedObjects)),
			eval(timeBeforeByMoreThan(encounterEventTime.get{HIGHLOW}(), $evalTime, {INT3}, {TIMEUNITS3}, namedObjects))
		),
	init (ArrayList $EBTCI_TempFinalEncIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3} = new ArrayList(); ),
	action ( $EBTCI_TempFinalEncIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}.add($EBTCI_FinalEncId_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}); ),
	reverse ( $EBTCI_TempFinalEncIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}.remove($EBTCI_FinalEncId_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}); ),
	result( $EBTCI_TempFinalEncIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3})
	) and  
	
	{ENCLISTID} : java.util.List( size >= {INT1} ) from collect 
	( 
		EncounterEvent
		(
			id memberOf $EBTCI_FinalEncIds_{INT1}{ENCLISTID}{ENCTYPE}{HIGHLOW}{INT2}{TIMEUNITS2}{INT3}{TIMEUNITS3}
		)
	) and
	EvaluatedPerson(eval(flagClinicalStatementListToReturnInOutput({ENCLISTID})))
) //DslUsed==Enc.Between.Times.Count.Id.Dsl|||INT1=={INT1}|||ENCLISTID=={ENCLISTID}|||ENCTYPE=={ENCTYPE}|||HIGHLOW=={HIGHLOW}|||INT2=={INT2}|||TIMEUNITS2=={TIMEUNITS2}|||INT3=={INT3}|||TIMEUNITS3=={TIMEUNITS3}