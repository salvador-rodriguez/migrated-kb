[when] Enc.Perf.ProvType.Time.Count - Evaluated Person had at least {INT1} {ENCTYPE:ENUM:EncounterTypeConcept.openCdsConceptCode} by {ENTTYPE:ENUM:EntityTypeConcept.openCdsConceptCode} who is {ENTREL:ENUM:EntityRelationshipConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in the past {INT2} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	$EncounterProviderTimeCountDsl_EncounterTypeConcept_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	( 
	EncounterTypeConcept
		( 
		openCdsConceptCode == "{ENCTYPE}",
		$EncounterTypeConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : conceptTargetId 
		),
	init (ArrayList $EncounterIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($EncounterIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EncounterTypeConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($EncounterIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EncounterTypeConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($EncounterIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$EncounterProviderTimeCountDsl_RelatedProviderConcept_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	( 
	EntityTypeConcept
		( 
		openCdsConceptCode == "{ENTTYPE}",
		$EntityTypeConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : conceptTargetId 
		),
	init (ArrayList $EntityIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($EntityIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EntityTypeConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($EntityIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EntityTypeConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($EntityIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$EncounterProviderTimeCountDsl_EntityRelationshipConcept_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	(
	EntityRelationshipConcept
		( 
		openCdsConceptCode == "{ENTREL}",
		$EntityRelationshipConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : conceptTargetId 
		),
	init (ArrayList $ERCIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($ERCIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EntityRelationshipConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($ERCIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EntityRelationshipConceptTargetId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($ERCIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$EncounterProviderTimeCountDsl_EntityRelationship_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List (size >= 0 ) from accumulate 
	(
	EntityRelationship
		( 
		id memberOf $EncounterProviderTimeCountDsl_EntityRelationshipConcept_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS},
		$EntityRelationshipSourceId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : sourceId memberOf $EncounterProviderTimeCountDsl_EncounterTypeConcept_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}, 
		targetEntityId memberOf $EncounterProviderTimeCountDsl_RelatedProviderConcept_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}
		),
	init (ArrayList $ERSourceIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} = new ArrayList(); ),
	action ($ERSourceIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.add($EntityRelationshipSourceId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	reverse ($ERSourceIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}.remove($EntityRelationshipSourceId_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}); ),
	result($ERSourceIds_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})
	) and  

	$EncounterProviderTimeCountDsl_EncounterEventList_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS} : java.util.List( size >= {INT1} ) from collect 
	( 
	EncounterEvent
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $EncounterProviderTimeCountDsl_EntityRelationship_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS}, 
		eval(timeBeforeByAtMost(EncounterEventTime.get{HIGHLOW}(), $evalTime, {INT2}, {TIMEUNITS}, namedObjects))			
		)
	) and

	EvaluatedPerson(eval(flagClinicalStatementListToReturnInOutput($EncounterProviderTimeCountDsl_EncounterEventList_{INT1}{ENCTYPE}{ENTREL}{ENTTYPE}_{INT2}{TIMEUNITS})))
) //DslUsed==Enc.Perf.ProvType.Time.Count.Dsl|||INT1=={INT1}|||ENCTYPE=={ENCTYPE}|||ENTREL=={ENTREL}|||ENTTYPE=={ENTTYPE}|||INT2=={INT2}|||TIMEUNITS=={TIMEUNITS}|||HIGHLOW=={HIGHLOW}