[when] EncDx.Count.Time - Evaluated Person had {PROB:ENUM:ProblemConcept.openCdsConceptCode} {INT1} or more times with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in the past {INT2} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	$PCT_problemConcepts_{PROB} : java.util.List (size >= {INT1}) from accumulate  
	( 
	ProblemConcept
		( 
		openCdsConceptCode == "{PROB}", 
		$ProblemConceptTargetId : conceptTargetId 
		),
	init (ArrayList $ProblemIds = new ArrayList(); ),
	action ($ProblemIds.add($ProblemConceptTargetId); ),
	reverse ($ProblemIds.remove($ProblemConceptTargetId); ),
	result($ProblemIds)		
	) and 

	$PCT_problems_{PROB} : java.util.List  (size >= {INT1}) from collect 
	( 
	Problem
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf 
			(
			$PCT_problemConcepts_{PROB}
			), 
		eval((templateId != null) && java.util.Arrays.asList(templateId).contains("2.16.840.1.113883.3.1829.11.7.2.18")),
		eval(timeBeforeByAtMost(problemEffectiveTime.get{HIGHLOW}(), $evalTime, {INT2}, {TIMEUNITS}, namedObjects))
		)
	) and

	EvaluatedPerson(eval(flagClinicalStatementListToReturnInOutput($PCT_problems_{PROB})))
) //DslUsed==EncDx.Count.Time.Dsl|||PROB=={PROB}|||INT1=={INT1}|||INT2=={INT2}|||TIMEUNITS=={TIMEUNITS}|||HIGHLOW=={HIGHLOW}