[when] EncDx.Past - Evaluated Person had {PROB:ENUM:ProblemConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time at any point prior to the Eval Time = 
(
	$EncDxPastDsl_problemConcept_{PROB} : ProblemConcept
	( 
	openCdsConceptCode == "{PROB}" 
	) and 

	$EncDxPastDsl_problem_{PROB} : Problem
	(
	id == $EncDxPastDsl_problemConcept_{PROB}.conceptTargetId,
	eval((templateId != null) && java.util.Arrays.asList(templateId).contains("2.16.840.1.113883.3.1829.11.7.2.18")),
	evaluatedPersonId == $evaluatedPersonId, 
	problemEffectiveTime.get{HIGHLOW}() < $evalTime
	) and

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($EncDxPastDsl_problem_{PROB})))
) //DslUsed==EncDx.Past.Dsl|||PROB=={PROB}|||HIGHLOW=={HIGHLOW}