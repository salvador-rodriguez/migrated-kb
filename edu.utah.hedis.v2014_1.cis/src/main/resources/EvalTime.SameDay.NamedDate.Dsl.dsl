[when] EvalTime.SameDay.NamedDate - Eval Time is same day as Named Date {NAME} when ignoring hours, minutes, and seconds = 
(
	NamedDate(name == "{NAME}", 
		eval(namedObjects.get("{NAME}") != null), 
		eval(org.opencds.common.utilities.DateUtility.getInstance().isSameDay($evalTime, ((java.util.Date) namedObjects.get("{NAME}")))))
	// the first line above is inserted SOLELY for the purpose of this evaluation being conducted when the NamedDate is inserted into memory in the same flow group where this DSL is used

) //DslUsed==EvalTime.SameDay.NamedDate.Dsl|||NAME=={NAME}