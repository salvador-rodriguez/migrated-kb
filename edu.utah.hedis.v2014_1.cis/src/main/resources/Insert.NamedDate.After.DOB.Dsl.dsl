[then] Insert.NamedDate.After.DOB - Insert Named Date "{NAME}" for both Rules and Process which is {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnit} after DOB; requires Read.DOB = 
java.util.Date namedDateValue = org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime( $ReadDOBDsl__DOB, {TIMEUNITS}, {INT});
namedObjects.put("{NAME}", namedDateValue);

NamedDate  $namedDateAfterDOB{INT}{TIMEUNITS} = new NamedDate ();
$namedDateAfterDOB{INT}{TIMEUNITS}.setName("{NAME}");
$namedDateAfterDOB{INT}{TIMEUNITS}.setDate(namedDateValue); 
insert ($namedDateAfterDOB{INT}{TIMEUNITS}); //DslUsed==Insert.NamedDate.After.DOB.Dsl|||NAME=={NAME}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}

