[then] Insert.NamedDate.After.NamedDate - Insert Named Date "{NAME}" for both Rules and Process which is {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnit} after {NAMEDDATENAME}; requires NamedDate.Exists.Not.Null = 
java.util.Date sourceNamedDateValue = (java.util.Date) namedObjects.get("{NAMEDDATENAME}");
java.util.Date namedDateValue = org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime(sourceNamedDateValue, {TIMEUNITS}, {INT});
namedObjects.put("{NAME}", namedDateValue);

// System.out.println(">> Adding: <{NAME}> with value " + namedDateValue.toString()); 

NamedDate $namedDateAfterNamedDate{INT}{TIMEUNITS} = new NamedDate();
$namedDateAfterNamedDate{INT}{TIMEUNITS}.setName("{NAME}");
$namedDateAfterNamedDate{INT}{TIMEUNITS}.setDate(namedDateValue); 
insert ($namedDateAfterNamedDate{INT}{TIMEUNITS}); //DslUsed==Insert.NamedDate.After.NamedDate.Dsl|||NAME=={NAME}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}|||NAMEDDATENAME=={NAMEDDATENAME}