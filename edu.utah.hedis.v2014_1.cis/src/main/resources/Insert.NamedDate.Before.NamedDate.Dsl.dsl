[then] Insert.NamedDate.Before.NamedDate - Insert Named Date "{NAME}" for both Rules and Process which is {INT} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnit} before {NAMEDDATENAME}; requires NamedDate.Exists.Not.Null = 
java.util.Date sourceNamedDateValue = (java.util.Date) namedObjects.get("{NAMEDDATENAME}");
java.util.Date namedDateValue = org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime(sourceNamedDateValue, {TIMEUNITS}, -1 * {INT});
namedObjects.put("{NAME}", namedDateValue);

// System.out.println(">> Adding: <{NAME}> with value " + namedDateValue.toString()); 

NamedDate $namedDateBeforeNamedDate{INT}{TIMEUNITS} = new NamedDate();
$namedDateBeforeNamedDate{INT}{TIMEUNITS}.setName("{NAME}");
$namedDateBeforeNamedDate{INT}{TIMEUNITS}.setDate(namedDateValue); 
insert ($namedDateBeforeNamedDate{INT}{TIMEUNITS}); //DslUsed==Insert.NamedDate.Before.NamedDate.Dsl|||NAME=={NAME}|||INT=={INT}|||TIMEUNITS=={TIMEUNITS}|||NAMEDDATENAME=={NAMEDDATENAME}