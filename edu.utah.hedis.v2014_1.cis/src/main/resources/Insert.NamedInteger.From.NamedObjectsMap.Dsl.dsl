[then] Insert.NamedInteger.From.NamedObjectsMap - Take Integer entry in namedObjects called {NAME} and make it available in rules as a NamedInteger with same name = 

NamedInteger $namedInteger = new NamedInteger();
$namedInteger.setName("{NAME}");
$namedInteger.setValue( (Integer) namedObjects.get("{NAME}") ); 

insert ($namedInteger); //DslUsed==Insert.NamedInteger.From.NamedObjectsMap|||NAME=={NAME}