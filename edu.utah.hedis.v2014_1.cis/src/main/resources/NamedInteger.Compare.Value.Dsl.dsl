[when] NamedInteger.Compare.Value - There is a Named Integer {NAME} with Integer value {COMP:ENUM:Comparison.operator}  {INT} = 
(
	NamedInteger(name == "{NAME}",
		eval(namedObjects.get("{NAME}") != null), 
		eval((((Integer) namedObjects.get("{NAME}")).intValue()) {COMP} {INT})
	) 
	// the first line above is inserted SOLELY for the purpose of this evaluation being conducted when the NamedInteger is inserted into memory in the same flow group where this DSL is used
) 
//DslUsed==NamedInteger.Compare.Value.Dsl|||NAME=={NAME}|||COMP=={COMP}|||INT=={INT}