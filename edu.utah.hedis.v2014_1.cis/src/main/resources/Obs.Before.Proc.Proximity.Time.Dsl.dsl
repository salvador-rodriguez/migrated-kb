[when] Obs.Before.Proc.Proximity.Time - Evaluated Person had {OBSFOCUS:ENUM:ObservationFocusConcept.openCdsConceptCode} with {HIGHLOW1:ENUM:TimeInterval.highLowUpper} time in the past {INT1} {TIMEUNITS1:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} which was followed by {PROC:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW2:ENUM:TimeInterval.highLowUpper} time within {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} inclusive = 
(
// e.g.,  Evaluated Person had [Pregnancy Test] with [Low] time in the past [1 yr] which was followed by [X-ray] with [low] time within [7 days] inclusive

	$OBPPT_{OBSFOCUS}{PROC}_preIds: java.util.List (size >= 1 ) from accumulate 
	( 
		ObservationFocusConcept
		( 
		openCdsConceptCode == "{OBSFOCUS}",
		$OBPPT_{OBSFOCUS}{PROC}_preTargetId : conceptTargetId 
		),
	init (ArrayList $OBPPT_{OBSFOCUS}{PROC}_tempPreIds = new ArrayList(); ),
	action ($OBPPT_{OBSFOCUS}{PROC}_tempPreIds.add($OBPPT_{OBSFOCUS}{PROC}_preTargetId); ),
	reverse ($OBPPT_{OBSFOCUS}{PROC}_tempPreIds.remove($OBPPT_{OBSFOCUS}{PROC}_preTargetId); ),
	result($OBPPT_{OBSFOCUS}{PROC}_tempPreIds)
	) and

	$OBPPT_{OBSFOCUS}{PROC}_postIds: java.util.List (size >= 1 ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$OBPPT_{OBSFOCUS}{PROC}_postTargetId : conceptTargetId 
		),
	init (ArrayList $OBPPT_{OBSFOCUS}{PROC}_tempPostIds = new ArrayList(); ),
	action ($OBPPT_{OBSFOCUS}{PROC}_tempPostIds.add($OBPPT_{OBSFOCUS}{PROC}_postTargetId); ),
	reverse ($OBPPT_{OBSFOCUS}{PROC}_tempPostIds.remove($OBPPT_{OBSFOCUS}{PROC}_postTargetId); ),
	result($OBPPT_{OBSFOCUS}{PROC}_tempPostIds)
	) and

	$OBPPT_{OBSFOCUS}{PROC}_preDates: java.util.List (size >= 1 ) from accumulate 
	( 
		$OBPPT_{OBSFOCUS}{PROC}_obsResult : ObservationResult
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $OBPPT_{OBSFOCUS}{PROC}_preIds,
		eval(timeBeforeByAtMost(observationEventTime.get{HIGHLOW1}(), $evalTime, {INT1}, {TIMEUNITS1}, namedObjects)),
		$OBPPT_{OBSFOCUS}{PROC}_preDate : observationEventTime.get{HIGHLOW1}()
		),
	init (ArrayList $OBPPT_{OBSFOCUS}{PROC}_tempPreDates = new ArrayList(); ),
	action ($OBPPT_{OBSFOCUS}{PROC}_tempPreDates.add($OBPPT_{OBSFOCUS}{PROC}_preDate); flagClinicalStatementToReturnInOutput($OBPPT_{OBSFOCUS}{PROC}_obsResult); ),
	reverse ($OBPPT_{OBSFOCUS}{PROC}_tempPreDates.remove($OBPPT_{OBSFOCUS}{PROC}_preDate); flagClinicalStatementToNotReturnInOutput($OBPPT_{OBSFOCUS}{PROC}_obsResult); ),
	result($OBPPT_{OBSFOCUS}{PROC}_tempPreDates)
	) and 

	$OBPPT_{OBSFOCUS}{PROC}_postDates: java.util.List (size >= 1 ) from accumulate 
	( 
		$OBPPT_{OBSFOCUS}{PROC}_proc : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $OBPPT_{OBSFOCUS}{PROC}_postIds,
		$OBPPT_{OBSFOCUS}{PROC}_postDate : procedureTime.get{HIGHLOW2}()
		),
	init (ArrayList $OBPPT_{OBSFOCUS}{PROC}_tempPostDates = new ArrayList(); ),
	action ($OBPPT_{OBSFOCUS}{PROC}_tempPostDates.add($OBPPT_{OBSFOCUS}{PROC}_postDate); flagClinicalStatementToReturnInOutput($OBPPT_{OBSFOCUS}{PROC}_proc); ),
	reverse ($OBPPT_{OBSFOCUS}{PROC}_tempPostDates.remove($OBPPT_{OBSFOCUS}{PROC}_postDate); flagClinicalStatementToNotReturnInOutput($OBPPT_{OBSFOCUS}{PROC}_proc); ),
	result($OBPPT_{OBSFOCUS}{PROC}_tempPostDates)
	) and	

	EvaluatedPerson(eval(timeAtOrBeforeByAtMost_ListEvaluation($OBPPT_{OBSFOCUS}{PROC}_preDates, $OBPPT_{OBSFOCUS}{PROC}_postDates, {INT2}, {TIMEUNITS2})))
) 
//DslUsed==Obs.Before.Proc.Proximity.Time.Dsl|||OBSFOCUS=={OBSFOCUS}|||HIGHLOW1=={HIGHLOW1}|||INT1=={INT1}|||TIMEUNITS1=={TIMEUNITS1}|||PROC=={PROC}|||HIGHLOW2=={HIGHLOW2}|||INT2=={INT2}|||TIMEUNITS2=={TIMEUNITS2}