[then] Out.Concepts.All.Write - Create output observation with ID {CONCEPTS} containing all OpenCDS Concepts in working memory selected above = 

IVLDate obsTime{CONCEPTS} = new IVLDate(); 
obsTime{CONCEPTS}.setLow($evalTime); 
obsTime{CONCEPTS}.setHigh($evalTime); 

ObservationResult parentObs{CONCEPTS} = new ObservationResult(); 
String parentObs{CONCEPTS}Id = "2.16.840.1.113883.3.795.5.1^{CONCEPTS}"; 
parentObs{CONCEPTS}.setId(parentObs{CONCEPTS}Id); 
parentObs{CONCEPTS}.setEvaluatedPersonId($evaluatedPersonId); 
parentObs{CONCEPTS}.setObservationEventTime(obsTime{CONCEPTS}); 
parentObs{CONCEPTS}.setSubjectIsFocalPerson($evaluatedPersonId == $focalPersonId); 

CD parentObs{CONCEPTS}Focus = new CD(); 
parentObs{CONCEPTS}Focus.setCodeSystem("2.16.840.1.113883.3.795.12.1.1"); 
parentObs{CONCEPTS}Focus.setCodeSystemName("OpenCDS concepts"); 
parentObs{CONCEPTS}Focus.setCode("concepts"); 
parentObs{CONCEPTS}Focus.setDisplayName("Concepts found in working memory"); 

parentObs{CONCEPTS}.setObservationFocus(parentObs{CONCEPTS}Focus); 
parentObs{CONCEPTS}.setClinicalStatementToBeRoot(true); 
parentObs{CONCEPTS}.setToBeReturned(true); 

ObservationValue obsValue{CONCEPTS} = new ObservationValue();

String conceptsAsString = getConceptsAsString($concepts);

obsValue{CONCEPTS}.setText(conceptsAsString);

parentObs{CONCEPTS}.setObservationValue(obsValue{CONCEPTS}); 
insert(parentObs{CONCEPTS}); 
namedObjects.put("parentObs{CONCEPTS}", (Object)parentObs{CONCEPTS}); //DslUsed==Out.Concepts.All.Write.Dsl|||CONCEPTS=={CONCEPTS}