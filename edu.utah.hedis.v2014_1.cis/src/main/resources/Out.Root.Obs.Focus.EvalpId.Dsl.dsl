[then] Out.Root.Obs.Focus.EvalpId - Create root output observation with ID {ROOT} focused on {OUTROOTFOCUS:ENUM:ObservationFocusConcept.openCdsConceptCode} with value identifying the evaluatedPerson = 

ObservationResult parentObs{ROOT} = new ObservationResult(); 
String parentObs{ROOT}Id = "2.16.840.1.113883.3.795.5.1^{ROOT}"; 

CD parentObs{ROOT}Focus = new CD(); 
parentObs{ROOT}Focus.setCodeSystem("2.16.840.1.113883.3.795.12.1.1"); 
parentObs{ROOT}Focus.setCodeSystemName("OpenCDS concepts"); 
parentObs{ROOT}Focus.setCode("{OUTROOTFOCUS}"); 
parentObs{ROOT}Focus.setDisplayName(getOpenCDSConceptName("{OUTROOTFOCUS}")); 

ObservationValue parentObs{ROOT}Value = new ObservationValue(); 
String parentObs{ROOT}Identifier = $evaluatedPersonId; 
parentObs{ROOT}Value.setIdentifier(parentObs{ROOT}Identifier); 

IVLDate obsTime{ROOT} = new IVLDate(); 
obsTime{ROOT}.setLow($evalTime); 
obsTime{ROOT}.setHigh($evalTime); 

parentObs{ROOT}.setId(parentObs{ROOT}Id); 
parentObs{ROOT}.setObservationFocus(parentObs{ROOT}Focus); 
parentObs{ROOT}.setObservationValue(parentObs{ROOT}Value); 
parentObs{ROOT}.setObservationEventTime(obsTime{ROOT}); 
parentObs{ROOT}.setEvaluatedPersonId($evaluatedPersonId); 
parentObs{ROOT}.setSubjectIsFocalPerson($evaluatedPersonId == $focalPersonId); 
parentObs{ROOT}.setClinicalStatementToBeRoot(true); 
parentObs{ROOT}.setToBeReturned(true); 

insert(parentObs{ROOT}); 
namedObjects.put("parentObs{ROOT}", parentObs{ROOT}); //DslUsed==Out.Root.Obs.Focus.EvalpId.Dsl|||ROOT=={ROOT}|||OUTROOTFOCUS=={OUTROOTFOCUS}