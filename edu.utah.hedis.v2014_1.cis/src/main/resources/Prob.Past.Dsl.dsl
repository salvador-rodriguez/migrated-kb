[when] Prob.Past - Evaluated Person had {PROB:ENUM:ProblemConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time at any point prior to the Eval Time = 
(
	$ProbPastDsl_problemConcept_{PROB} : ProblemConcept
	( 
	openCdsConceptCode == "{PROB}" 
	) and 

	$ProbPastDsl_problem_{PROB} : Problem
	(
	id == $ProbPastDsl_problemConcept_{PROB}.conceptTargetId,
	evaluatedPersonId == $evaluatedPersonId, 
	problemEffectiveTime.get{HIGHLOW}() < $evalTime
	) and

	EvaluatedPerson(eval(flagClinicalStatementToReturnInOutput($ProbPastDsl_problem_{PROB})))

) //DslUsed==Prob.Past.Dsl|||PROB=={PROB}|||HIGHLOW=={HIGHLOW}