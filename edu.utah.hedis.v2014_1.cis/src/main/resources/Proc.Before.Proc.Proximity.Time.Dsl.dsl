[when] Proc.Before.Proc.Proximity.Time - Evaluated Person had {PROC1:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW1:ENUM:TimeInterval.highLowUpper} time in the past {INT1} {TIMEUNITS1:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} which was followed by {PROC2:ENUM:ProcedureConcept.openCdsConceptCode} with {HIGHLOW2:ENUM:TimeInterval.highLowUpper} time within {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} inclusive = 
(
// e.g.,  Evaluated Person had [Pregnancy Test] with [Low] time in the past [1 yr] which was followed by [X-ray] with [low] time within [7 days] inclusive

	$PBPPT{PROC1}{PROC2}_preIds: java.util.List (size >= 1 ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC1}",
		$PBPPT{PROC1}{PROC2}_preTargetId : conceptTargetId 
		),
	init (ArrayList $PBPPT{PROC1}{PROC2}_tempPreIds = new ArrayList(); ),
	action ($PBPPT{PROC1}{PROC2}_tempPreIds.add($PBPPT{PROC1}{PROC2}_preTargetId); ),
	reverse ($PBPPT{PROC1}{PROC2}_tempPreIds.remove($PBPPT{PROC1}{PROC2}_preTargetId); ),
	result($PBPPT{PROC1}{PROC2}_tempPreIds)
	) and

	$PBPPT{PROC1}{PROC2}_postIds: java.util.List (size >= 1 ) from accumulate 
	( 
		ProcedureConcept
		( 
		openCdsConceptCode == "{PROC2}",
		$PBPPT{PROC1}{PROC2}_postTargetId : conceptTargetId 
		),
	init (ArrayList $PBPPT{PROC1}{PROC2}_tempPostIds = new ArrayList(); ),
	action ($PBPPT{PROC1}{PROC2}_tempPostIds.add($PBPPT{PROC1}{PROC2}_postTargetId); ),
	reverse ($PBPPT{PROC1}{PROC2}_tempPostIds.remove($PBPPT{PROC1}{PROC2}_postTargetId); ),
	result($PBPPT{PROC1}{PROC2}_tempPostIds)
	) and

	$PBPPT{PROC1}{PROC2}_preDates: java.util.List (size >= 1 ) from accumulate 
	( 
		$PBPPT{PROC1}{PROC2}_proc1 : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PBPPT{PROC1}{PROC2}_preIds,
		eval(timeBeforeByAtMost(procedureTime.get{HIGHLOW1}(), $evalTime, {INT1}, {TIMEUNITS1}, namedObjects)),
		$PBPPT{PROC1}{PROC2}_preDate : procedureTime.get{HIGHLOW1}()
		),
	init (ArrayList $PBPPT{PROC1}{PROC2}_tempPreDates = new ArrayList(); ),
	action ($PBPPT{PROC1}{PROC2}_tempPreDates.add($PBPPT{PROC1}{PROC2}_preDate); flagClinicalStatementToReturnInOutput($PBPPT{PROC1}{PROC2}_proc1); ),
	reverse ($PBPPT{PROC1}{PROC2}_tempPreDates.remove($PBPPT{PROC1}{PROC2}_preDate); flagClinicalStatementToNotReturnInOutput($PBPPT{PROC1}{PROC2}_proc1); ),
	result($PBPPT{PROC1}{PROC2}_tempPreDates)
	) and 

	$PBPPT{PROC1}{PROC2}_postDates: java.util.List (size >= 1 ) from accumulate 
	( 
		$PBPPT{PROC1}{PROC2}_proc2 : ProcedureEvent
		( 
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PBPPT{PROC1}{PROC2}_postIds,
		$PBPPT{PROC1}{PROC2}_postDate : procedureTime.get{HIGHLOW2}()
		),
	init (ArrayList $PBPPT{PROC1}{PROC2}_tempPostDates = new ArrayList(); ),
	action ($PBPPT{PROC1}{PROC2}_tempPostDates.add($PBPPT{PROC1}{PROC2}_postDate); flagClinicalStatementToReturnInOutput(		$PBPPT{PROC1}{PROC2}_proc2); ),
	reverse ($PBPPT{PROC1}{PROC2}_tempPostDates.remove($PBPPT{PROC1}{PROC2}_postDate); flagClinicalStatementToNotReturnInOutput(		$PBPPT{PROC1}{PROC2}_proc2); ),
	result($PBPPT{PROC1}{PROC2}_tempPostDates)
	) and	

	EvaluatedPerson(eval(timeAtOrBeforeByAtMost_ListEvaluation($PBPPT{PROC1}{PROC2}_preDates, $PBPPT{PROC1}{PROC2}_postDates, {INT2}, {TIMEUNITS2})))
) //DslUsed==Proc.Before.Proc.Proximity.Time.Dsl|||PROC1=={PROC1}|||HIGHLOW1=={HIGHLOW1}|||INT1=={INT1}|||TIMEUNITS1=={TIMEUNITS1}|||PROC2=={PROC2}|||HIGHLOW2=={HIGHLOW2}|||INT2=={INT2}|||TIMEUNITS2=={TIMEUNITS2}