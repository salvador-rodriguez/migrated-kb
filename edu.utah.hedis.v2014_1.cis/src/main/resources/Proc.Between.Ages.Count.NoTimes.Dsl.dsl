[when] Proc.Between.Ages.Count.NoTimes - Evaluated Person has count {NAME} of {PROC:ENUM:ProcedureConcept.openCdsConceptCode} in the past with {HIGHLOW:ENUM:TimeInterval.highLowUpper} date {COMP1:ENUM:Comparison.operator} DOB plus {INT1} {TIMEUNITS1:ENUM:EnumerationTarget.javaCalendarUnit} and {COMP2:ENUM:Comparison.operator} DOB plus {INT2} {TIMEUNITS2:ENUM:EnumerationTarget.javaCalendarUnit} ignoring time components of all dates = 
(
	$ProcBetweenAgesCountNoTimesDsl_ProcedureConcept_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1} : java.util.List (size >= 0 ) from accumulate 
	( 
	ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$ProcedureConceptTargetId_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1} : conceptTargetId 
		),
	init (List $ProcedureIds_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1} = new ArrayList(); ),
	action ($ProcedureIds_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}.add($ProcedureConceptTargetId_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}); ),
	reverse ($ProcedureIds_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}.remove($ProcedureConceptTargetId_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}); ),
	result($ProcedureIds_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1})
	) and  

	EvaluatedPerson(id == $evaluatedPersonId, demographics != null, demographics.birthTime!= null, $ProcBetweenAgesCountNoTimesDsl_DOB_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1} : demographics.birthTime) and
	
	$ProcBetweenAgesCountNoTimesDsl_ProcedureEvent_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1} : java.util.List( size >= 0 ) from collect 
	( 
	ProcedureEvent
		(
		id memberOf $ProcBetweenAgesCountNoTimesDsl_ProcedureConcept_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}, 
		evaluatedPersonId == $evaluatedPersonId,
		procedureTime.get{HIGHLOW}() < $evalTime,
		(stripTimeComponent(procedureTime.get{HIGHLOW}()) ) {COMP1} 
		(stripTimeComponent(org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime(
				$ProcBetweenAgesCountNoTimesDsl_DOB_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}, 
				{TIMEUNITS1}, 
				{INT1})
			) ),
		(stripTimeComponent(procedureTime.get{HIGHLOW}()) ) {COMP2} 
		(stripTimeComponent(org.opencds.common.utilities.DateUtility.getInstance().getDateAfterAddingTime(
				$ProcBetweenAgesCountNoTimesDsl_DOB_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}, 
				{TIMEUNITS2}, 
				{INT2})
			) )
		)
	)  and 
	
	EvaluatedPerson(
		eval(addNamedObject
		( 
		"{NAME}", Integer.valueOf(
		"" + ((List)$ProcBetweenAgesCountNoTimesDsl_ProcedureEvent_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1} ).size()), 
		namedObjects 
		)),	eval(flagClinicalStatementListToReturnInOutput($ProcBetweenAgesCountNoTimesDsl_ProcedureEvent_{NAME}{PROC}_{INT1}_{INT2}_{TIMEUNITS1}))
	)
) //DslUsed==Proc.Between.Ages.Count.NoTimes.Dsl|||NAME=={NAME}|||PROC=={PROC}|||INT1=={INT1}|||INT2=={INT2}|||TIMEUNITS1=={TIMEUNITS1}|||TIMEUNITS2=={TIMEUNITS2}|||HIGHLOW=={HIGHLOW}|||COMP1=={COMP1}|||COMP2=={COMP2}