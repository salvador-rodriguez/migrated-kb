[when] Proc.Count.Time - Evaluated Person had {PROC:ENUM:ProcedureConcept.openCdsConceptCode} {INT1} or more times with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time in the past {INT2} {TIMEUNITS:ENUM:EnumerationTarget.javaCalendarUnitNoWeeks} = 
(
	$PCT_procedureConcepts_{PROC} : java.util.List (size >= {INT1}) from accumulate
	( 
	ProcedureConcept
		(
		openCdsConceptCode == "{PROC}", 
		$ProcedureConceptTargetId : conceptTargetId 
		),
	init (ArrayList $ProcedureIds = new ArrayList(); ),
	action ($ProcedureIds.add($ProcedureConceptTargetId); ),
	reverse ($ProcedureIds.remove($ProcedureConceptTargetId); ),
	result($ProcedureIds)		
	) and 
	
	$PCT_procedureEventList_{PROC} : java.util.List (  size >= {INT1}  ) from collect 
	( 
	ProcedureEvent
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PCT_procedureConcepts_{PROC}, 
		eval(timeBeforeByAtMost(procedureTime.get{HIGHLOW}(), $evalTime, {INT2}, {TIMEUNITS}, namedObjects))
		)
	) and
	
	EvaluatedPerson(eval(flagClinicalStatementListToReturnInOutput($PCT_procedureEventList_{PROC})))
	
) //DslUsed==Proc.Count.Time.Dsl|||PROC=={PROC}|||INT1=={INT1}|||INT2=={INT2}|||TIMEUNITS=={TIMEUNITS}|||HIGHLOW=={HIGHLOW}