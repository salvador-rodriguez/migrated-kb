[when] Proc.Perf.ProvType.Between.NamedDates.NoTimes.Count - Evaluated Person had at least {INT} {PROC:ENUM:ProcedureConcept.openCdsConceptCode} by {ENTTYPE:ENUM:EntityTypeConcept.openCdsConceptCode} who is {ENTREL:ENUM:EntityRelationshipConcept.openCdsConceptCode} with {HIGHLOW:ENUM:TimeInterval.highLowUpper} time {COMP1:ENUM:Comparison.operatorGTE} {NAMEDDATE1NAME} and {COMP2:ENUM:Comparison.operatorLTE} {NAMEDDATE2NAME} ignoring time components of all dates = 
(
	$PPPTNDNTC_NamedDate1_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}  : NamedDate
	(
		name == "{NAMEDDATE1NAME}", 
		date != null,
		$PPPTNDNTC_NamedDate1Value_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE1NAME}"))
	) and
	
	$PPPTNDNTC_NamedDate2_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}  : NamedDate
	(
		name == "{NAMEDDATE2NAME}", 
		date != null,
		$PPPTNDNTC_NamedDate2Value_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : date == ((java.util.Date) namedObjects.get("{NAMEDDATE2NAME}"))
	) and
	
	$PPPTNDNTC_ProcedureConcept_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : java.util.List (size >= 0 ) from accumulate 
	( 
	ProcedureConcept
		( 
		openCdsConceptCode == "{PROC}",
		$ProcedureConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : conceptTargetId 
		),
	init (ArrayList $ProcedureIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} = new ArrayList(); ),
	action ($ProcedureIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.add($ProcedureConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	reverse ($ProcedureIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.remove($ProcedureConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	result($ProcedureIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW})
	) and  

	$PPPTNDNTC_RelatedProviderConcept_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : java.util.List (size >= 0 ) from accumulate 
	( 
	EntityTypeConcept
		( 
		openCdsConceptCode == "{ENTTYPE}",
		$EntityTypeConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : conceptTargetId 
		),
	init (ArrayList $EntityIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} = new ArrayList(); ),
	action ($EntityIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.add($EntityTypeConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	reverse ($EntityIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.remove($EntityTypeConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	result($EntityIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW})
	) and  

	$PPPTNDNTC_EntityRelationshipConcept_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : java.util.List (size >= 0 ) from accumulate 
	(
	EntityRelationshipConcept
		( 
		openCdsConceptCode == "{ENTREL}",
		$EntityRelationshipConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : conceptTargetId 
		),
	init (ArrayList $ERCIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} = new ArrayList(); ),
	action ($ERCIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.add($EntityRelationshipConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	reverse ($ERCIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.remove($EntityRelationshipConceptTargetId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	result($ERCIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW})
	) and  

	$PPPTNDNTC_EntityRelationship_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : java.util.List (size >= 0 ) from accumulate 
	(
	EntityRelationship
		( 
		id memberOf $PPPTNDNTC_EntityRelationshipConcept_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW},
		$EntityRelationshipSourceId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : sourceId memberOf $PPPTNDNTC_ProcedureConcept_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}, 
		targetEntityId memberOf $PPPTNDNTC_RelatedProviderConcept_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}
		),
	init (ArrayList $ERSourceIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} = new ArrayList(); ),
	action ($ERSourceIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.add($EntityRelationshipSourceId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	reverse ($ERSourceIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}.remove($EntityRelationshipSourceId_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}); ),
	result($ERSourceIds_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW})
	) and  

	$PPPTNDNTC_ProcedureEventList_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW} : java.util.List( size >= {INT} ) from collect 
	( 
	ProcedureEvent
		(
		evaluatedPersonId == $evaluatedPersonId, 
		id memberOf $PPPTNDNTC_EntityRelationship_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}, 		
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP1} (stripTimeComponent($PPPTNDNTC_NamedDate1Value_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW})),
		(stripTimeComponent(procedureTime.get{HIGHLOW}())) {COMP2} (stripTimeComponent($PPPTNDNTC_NamedDate2Value_{INT}{PROC}{ENTTYPE}{ENTREL}{HIGHLOW}))
		)
	)
) //DslUsed==Proc.Perf.ProvType.Between.NamedDates.NoTimes.Count.Dsl|||INT=={INT}|||PROC=={PROC}||||||ENTTYPE=={ENTTYPE}|||ENTREL=={ENTREL}|||HIGHLOW=={HIGHLOW}|||COMP1=={COMP1}|||NAMEDDATE1NAME=={NAMEDDATE1NAME}|||COMP2=={COMP2}|||NAMEDDATE2NAME=={NAMEDDATE2NAME}