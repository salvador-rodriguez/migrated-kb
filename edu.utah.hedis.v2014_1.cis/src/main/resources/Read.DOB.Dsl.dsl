[when] Read.DOB - Get DOB =
(
	EvaluatedPerson(id == $evaluatedPersonId, demographics != null, demographics.birthTime!= null, $ReadDOBDsl__DOB : demographics.birthTime) 
) //DslUsed==Read.DOB.Dsl