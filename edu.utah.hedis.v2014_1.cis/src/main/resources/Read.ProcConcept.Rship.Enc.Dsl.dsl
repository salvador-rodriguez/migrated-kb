[when] Read.ProcConcept.Rship.Enc - Get Procedure Concept from Procedure that is {CSREL:ENUM:ClinicalStatementRelationshipConcept.openCdsConceptCode} Encounter =
(
	//select a specific relationship concept
	$RPCRE_clinicalStatementRelationshipConcept : ClinicalStatementRelationshipConcept
	(
		openCdsConceptCode == "{CSREL}" 
	) and
	
	//join to relationships on that concept
	$RPCRE_clinicalStatementRelationship : ClinicalStatementRelationship 
	( 
		id == $RPCRE_clinicalStatementRelationshipConcept.conceptTargetId
		//$RPCRE_clinicalStatementRelationshipSourceId : sourceId,
		//targetId == $RPCRE_procedureId
	) and

	//join to all procedureConcepts
	$RPCRE_procedureConcept : ProcedureConcept  						
	(
	) and
	
	//where procedureEvents have a procedureConcept
	ProcedureEvent														
	(
		$RPCRE_procedureId : id == $RPCRE_procedureConcept.conceptTargetId,
		id == $RPCRE_clinicalStatementRelationship.targetId,
		evaluatedPersonId == $evaluatedPersonId
	) and
	
	//join to encounterEvents with specified relationship to procedure
	EncounterEvent														
	(
		$RPCRE_encounterId : id == $RPCRE_clinicalStatementRelationship.sourceId, 
		evaluatedPersonId == $evaluatedPersonId,
		encounterEventTime.low < $evalTime
	) and not
	
	//where there is NOT an encounterTypeConcept matching procedureConcept
	EncounterTypeConcept												 
	(
		conceptTargetId == $RPCRE_encounterId, 
		openCdsConceptCode == $RPCRE_procedureConcept.openCdsConceptCode
	) 	
) //DslUsed==Read.ProcConcept.Rship.Enc.Dsl|||CSREL=={CSREL}