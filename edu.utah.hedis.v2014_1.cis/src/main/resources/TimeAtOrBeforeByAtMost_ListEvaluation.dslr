package edu.utah.hedis.v2014_1.cis;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.lang.Integer;
import org.opencds.common.ConsoleLogger;
import org.opencds.common.ILogger;
import org.opencds.common.utilities.AbsoluteTimeDifference;
import org.opencds.common.utilities.DateUtility;
import org.opencds.common.utilities.MiscUtility;
import org.opencds.common.utilities.StreamUtility;
import org.opencds.common.utilities.StringToArrayUtility;
import org.opencds.common.utilities.StringUtility;
import org.opencds.common.utilities.XMLDateUtility;
import org.opencds.vmr.v1_0.internal.utilities.LogicHelperUtility;
import org.opencds.vmr.v1_0.internal.AdministrableSubstance;
import org.opencds.vmr.v1_0.internal.AdverseEvent;
import org.opencds.vmr.v1_0.internal.AdverseEventBase;
import org.opencds.vmr.v1_0.internal.AppointmentProposal;
import org.opencds.vmr.v1_0.internal.AppointmentRequest;
import org.opencds.vmr.v1_0.internal.Assertion;
import org.opencds.vmr.v1_0.internal.BodySite;
import org.opencds.vmr.v1_0.internal.CDSContext;
import org.opencds.vmr.v1_0.internal.CDSInput;
import org.opencds.vmr.v1_0.internal.CDSOutput;
import org.opencds.vmr.v1_0.internal.CDSResource;
import org.opencds.vmr.v1_0.internal.ClinicalStatement;
import org.opencds.vmr.v1_0.internal.ClinicalStatementRelationship;
import org.opencds.vmr.v1_0.internal.Demographics;
import org.opencds.vmr.v1_0.internal.DeniedAdverseEvent;
import org.opencds.vmr.v1_0.internal.DeniedProblem;
import org.opencds.vmr.v1_0.internal.DoseRestriction;
import org.opencds.vmr.v1_0.internal.DosingSigElement;
import org.opencds.vmr.v1_0.internal.EncounterBase;
import org.opencds.vmr.v1_0.internal.EncounterEvent;
import org.opencds.vmr.v1_0.internal.Entity;
import org.opencds.vmr.v1_0.internal.EntityBase;
import org.opencds.vmr.v1_0.internal.EntityRelationship;
import org.opencds.vmr.v1_0.internal.EvalTime;
import org.opencds.vmr.v1_0.internal.EvaluatedPerson;
import org.opencds.vmr.v1_0.internal.EvaluatedPersonRelationship;
import org.opencds.vmr.v1_0.internal.EvaluatedPersonAgeAtEvalTime;
import org.opencds.vmr.v1_0.internal.Facility;
import org.opencds.vmr.v1_0.internal.FocalPersonId;
import org.opencds.vmr.v1_0.internal.Goal;
import org.opencds.vmr.v1_0.internal.GoalBase;
import org.opencds.vmr.v1_0.internal.GoalProposal;
import org.opencds.vmr.v1_0.internal.GoalValue;
import org.opencds.vmr.v1_0.internal.MissedAppointment;
import org.opencds.vmr.v1_0.internal.NamedObject;
import org.opencds.vmr.v1_0.internal.ObservationBase;
import org.opencds.vmr.v1_0.internal.ObservationOrder;
import org.opencds.vmr.v1_0.internal.ObservationProposal;
import org.opencds.vmr.v1_0.internal.ObservationResult;
import org.opencds.vmr.v1_0.internal.ObservationValue;
import org.opencds.vmr.v1_0.internal.Organization;
import org.opencds.vmr.v1_0.internal.OtherEvaluatedPerson;
import org.opencds.vmr.v1_0.internal.Person;
import org.opencds.vmr.v1_0.internal.Problem;
import org.opencds.vmr.v1_0.internal.ProblemBase;
import org.opencds.vmr.v1_0.internal.ProcedureBase;
import org.opencds.vmr.v1_0.internal.ProcedureEvent;
import org.opencds.vmr.v1_0.internal.ProcedureOrder;
import org.opencds.vmr.v1_0.internal.ProcedureProposal;
import org.opencds.vmr.v1_0.internal.RelationshipToSource;
import org.opencds.vmr.v1_0.internal.ScheduledAppointment;
import org.opencds.vmr.v1_0.internal.ScheduledProcedure;
import org.opencds.vmr.v1_0.internal.Specimen;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationBase;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationEvent;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationOrder;
import org.opencds.vmr.v1_0.internal.SubstanceAdministrationProposal;
import org.opencds.vmr.v1_0.internal.SubstanceDispensationEvent;
import org.opencds.vmr.v1_0.internal.SupplyBase;
import org.opencds.vmr.v1_0.internal.SupplyEvent;
import org.opencds.vmr.v1_0.internal.SupplyOrder;
import org.opencds.vmr.v1_0.internal.SupplyProposal;
import org.opencds.vmr.v1_0.internal.UnconductedObservation;
import org.opencds.vmr.v1_0.internal.UndeliveredProcedure;
import org.opencds.vmr.v1_0.internal.UndeliveredSubstanceAdministration;
import org.opencds.vmr.v1_0.internal.UndeliveredSupply;
import org.opencds.vmr.v1_0.internal.VMR;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.CDSInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.ClinicalStatementInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.CodedAttributeRequirement;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.EvaluatedPersonInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.PatientInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.RelatedEntityInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.RelatedEvaluatedPersonInputSpecification;
import org.opencds.vmr.v1_0.internal.cdsinputspecification.TimeAttributeRequirement;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventAffectedBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventAffectedBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventAgentConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventSeverityConcept;
import org.opencds.vmr.v1_0.internal.concepts.AdverseEventStatusConcept;
import org.opencds.vmr.v1_0.internal.concepts.BrandedMedicationConcept;
import org.opencds.vmr.v1_0.internal.concepts.CDSInputTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.CDSOutputTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.ClinicalStatementRelationshipConcept;
import org.opencds.vmr.v1_0.internal.concepts.ClinicalStatementTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.DataSourceTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.DoseTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.DosingSigConcept;
import org.opencds.vmr.v1_0.internal.concepts.EncounterCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.EncounterTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.EntityRelationshipConcept;
import org.opencds.vmr.v1_0.internal.concepts.EntityTemplateConcept;
import org.opencds.vmr.v1_0.internal.concepts.EntityTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.EthnicityConcept;
import org.opencds.vmr.v1_0.internal.concepts.EvaluatedPersonRelationshipConcept;
import org.opencds.vmr.v1_0.internal.concepts.GenderConcept;
import org.opencds.vmr.v1_0.internal.concepts.GenericMedicationConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalCodedValueConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalFocusConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalStatusConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.GoalTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ImmunizationConcept;
import org.opencds.vmr.v1_0.internal.concepts.InformationAttestationTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.InformationRecipientPreferredLanguageConcept;
import org.opencds.vmr.v1_0.internal.concepts.InformationRecipientTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.ManufacturerConcept;
import org.opencds.vmr.v1_0.internal.concepts.MedicationClassConcept;
import org.opencds.vmr.v1_0.internal.concepts.MedicationConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationCodedValueConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationFocusConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationInterpretationConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationMethodConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ObservationUnconductedReasonConcept;
import org.opencds.vmr.v1_0.internal.concepts.PreferredLanguageConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemAffectedBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemAffectedBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemImportanceConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemSeverityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProblemStatusConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureApproachBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureApproachBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureMethodConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.ProcedureTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.RaceConcept;
import org.opencds.vmr.v1_0.internal.concepts.ResourceTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationApproachBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationApproachBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationGeneralPurposeConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceAdministrationTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceDeliveryMethodConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceDeliveryRouteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SubstanceFormConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyCriticalityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyTargetBodySiteConcept;
import org.opencds.vmr.v1_0.internal.concepts.SupplyTargetBodySiteLateralityConcept;
import org.opencds.vmr.v1_0.internal.concepts.SystemUserPreferredLanguageConcept;
import org.opencds.vmr.v1_0.internal.concepts.SystemUserTaskContextConcept;
import org.opencds.vmr.v1_0.internal.concepts.SystemUserTypeConcept;
import org.opencds.vmr.v1_0.internal.concepts.UndeliveredProcedureReasonConcept;
import org.opencds.vmr.v1_0.internal.concepts.UndeliveredSubstanceAdministrationReasonConcept;
import org.opencds.vmr.v1_0.internal.concepts.VmrOpenCdsConcept;
import org.opencds.vmr.v1_0.internal.concepts.VMRTemplateConcept;
import org.opencds.vmr.v1_0.internal.datatypes.AddressPartType;
import org.opencds.vmr.v1_0.internal.datatypes.ADXP;
import org.opencds.vmr.v1_0.internal.datatypes.ANY;
import org.opencds.vmr.v1_0.internal.datatypes.AD;
import org.opencds.vmr.v1_0.internal.datatypes.BL;
import org.opencds.vmr.v1_0.internal.datatypes.CD;
import org.opencds.vmr.v1_0.internal.datatypes.CS;
import org.opencds.vmr.v1_0.internal.datatypes.EntityNamePartQualifier;
import org.opencds.vmr.v1_0.internal.datatypes.EntityNamePartType;
import org.opencds.vmr.v1_0.internal.datatypes.EntityNameUse;
import org.opencds.vmr.v1_0.internal.datatypes.EN;
import org.opencds.vmr.v1_0.internal.datatypes.ENXP;
import org.opencds.vmr.v1_0.internal.datatypes.II;
import org.opencds.vmr.v1_0.internal.datatypes.INT;
import org.opencds.vmr.v1_0.internal.datatypes.IVLDate;
import org.opencds.vmr.v1_0.internal.datatypes.IVLPQ;
import org.opencds.vmr.v1_0.internal.datatypes.IVLINT;
import org.opencds.vmr.v1_0.internal.datatypes.IVLQTY;
import org.opencds.vmr.v1_0.internal.datatypes.IVLREAL;
import org.opencds.vmr.v1_0.internal.datatypes.IVLRTO;
import org.opencds.vmr.v1_0.internal.datatypes.PostalAddressUse;
import org.opencds.vmr.v1_0.internal.datatypes.PQ;
import org.opencds.vmr.v1_0.internal.datatypes.QTY;
import org.opencds.vmr.v1_0.internal.datatypes.REAL;
import org.opencds.vmr.v1_0.internal.datatypes.RTO;
import org.opencds.vmr.v1_0.internal.datatypes.TEL;
import org.opencds.vmr.v1_0.internal.datatypes.TelecommunicationAddressUse;
import org.opencds.vmr.v1_0.internal.datatypes.TelecommunicationCapability;
import org.opencds.vmr.v1_0.internal.datatypes.UncertaintyType;
import org.opencds.vmr.v1_0.internal.datatypes.XP;

function boolean timeAtOrBeforeByAtMost_ListEvaluation(java.util.List timeList1, java.util.List timeList2, int maxAmountTime1BeforeTime2, int timeUnits) 
{
/* 
Pre-conditions: 
- timeUnits must be in Calendar enumeration times.  Currently does not support "Weeks". 
- timeList1 and timeList2 contain java.util.Date objects (may be empty)

Evaluates whether there is at least one entry where time from list 1 is at or before time from list 2.  

Uses the DateUtility's timeDifferenceLessThanOrEqualTo function for determining proximity, which means that hours, minutes, and seconds are ignored if the timeUnits selected is days or above.

Time1 must be <= Time2 when accounting for hours, minutes, and seconds.

Time1 + interval must be <= Time2.  Here, hours, minutes and seconds are ignored if  time interval is days, months, or years.

E.g., if date difference of 7 days specified, will return true if there is any entry in timeList1 which is (1) same time as any entry in timeList2, or (2) any entry in timeList2 occurred any time on the 7th day following time1 (e.g., if time1 was 1/1/2012, any time on 1/1/2012-1/8/2012 for time2 would result in a true evaluation)

*/
	org.opencds.common.utilities.DateUtility dateUtility = org.opencds.common.utilities.DateUtility.getInstance();

/* 
System.out.println();	
System.out.println("timeList1: " + timeList1.toString());
System.out.println("timeList2: " + timeList2.toString());
System.out.println(); 
*/

	if ((timeList1 != null)  && (timeList2 != null))
	{
		for (java.util.Date time1 : (java.util.List<java.util.Date>) timeList1)
		{
			for (java.util.Date time2 : (java.util.List<java.util.Date>) timeList2)
			{
				if (time1.equals(time2))
				{
					return true;
				}
				else
				{
					if (time1.before(time2))
					{
						if (dateUtility.timeDifferenceLessThanOrEqualTo(time1, time2, timeUnits, maxAmountTime1BeforeTime2))
						{
							return true;
						}
					}
				}
			}
		}
	}
	return false;		
}	